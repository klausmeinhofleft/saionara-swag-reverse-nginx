Swag repo https://github.com/linuxserver/docker-swag

Swag tutorial https://docs.linuxserver.io/general/swag

Swag default proxy settings https://github.com/linuxserver/docker-swag/blob/master/root/defaults/proxy.conf

Supported services out-of-the-box https://github.com/linuxserver/reverse-proxy-confs

Other tutorial on Swag https://blog.linuxserver.io/2017/11/28/how-to-setup-a-reverse-proxy-with-letsencrypt-ssl-for-all-your-docker-apps/

Using only NGinx (without all Swag features):
- https://github.com/dawilk/coderevolve-docker-reverseproxy
- https://appfleet.com/blog/reverse-proxy-with-docker-compose/

